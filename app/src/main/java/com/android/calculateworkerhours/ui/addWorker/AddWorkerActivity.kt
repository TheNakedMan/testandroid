package com.android.calculateworkerhours.ui.addWorker

import android.os.Bundle
import android.widget.ProgressBar
import com.android.calculateworkerhours.R
import com.android.calculateworkerhours.ui.BaseActivity
import kotlinx.android.synthetic.main.add_worker_activity.*

class AddWorkerActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_worker_activity)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun getProgressBar(): ProgressBar {
        return progressBar
    }


}