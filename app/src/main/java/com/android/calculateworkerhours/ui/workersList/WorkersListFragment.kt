package com.android.calculateworkerhours.ui.workersList

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.android.calculateworkerhours.R
import com.android.calculateworkerhours.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_workers_list.*
import org.koin.android.ext.android.inject

class WorkersListFragment : BaseFragment(R.layout.fragment_workers_list) {

    private val adapter = WorkersListAdapter()
    private val workersListViewModel: WorkersViewModel by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        addObserverToViewModel()
        addClickListener()
    }

    private fun addClickListener() {
       goToWorker.setOnClickListener {
           findNavController().navigate(R.id.action_workersList_to_addWorkerActivity)
       }
    }

    private fun initAdapter() {
        workersList.adapter = adapter
    }

    private fun addObserverToViewModel() {
       workersListViewModel.apply {
            getCurrentViewModel().getData().observe(viewLifecycleOwner, Observer {
                adapter.sedData(it)
            })
        }
    }

    override fun getCurrentViewModel(): WorkersViewModel {
        return workersListViewModel
    }

}


